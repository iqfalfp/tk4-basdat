from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import LoginForm, RegPenggunaForm, RegAdmSistemForm, RegAdmSatgasForm, RegDokterForm 
import json
from django.views.decorators.csrf import csrf_exempt
from datetime import date
from django.contrib.auth import authenticate
from django.db import connection
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.contrib import messages


def login(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data["email"]
                password = form.cleaned_data["password"]
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("SELECT * FROM  AKUN_PENGGUNA WHERE username = %s AND password = %s;", [username, password])
                
                account = dictfetchall(cursor)
                print(account)

                if len(account) != 0:
                    account = account[0]
                    request.session["username_account"] = account["username"]
                    request.session["password_account"] = account["password"]
                    request.session["role_account"] = account["peran"]

                    if request.session["role_account"] == "admin satgas":
                        print("admin satgas")
                        return redirect('user_profile:admin_satgas_profile')
                    elif request.session["role_account"] == "pengpub":
                        return redirect('user_profile:pengguna_publik_profile')
                        print("pengguna publik")
                    elif request.session["role_account"] == "admin sistem":
                        return redirect('user_profile:admin_sistem_profile')
                        print("admin sistem")
                    elif request.session["role_account"] == "dokter":
                        return redirect('user_profile:dokter_profile')
                        print("dokter") 

                messages.error(request, "username or password invalid")
                return redirect('users:login')

        else:
            form = LoginForm()
            return render(request, "login.html", {'form':form})
      
 #--- logout ---#
def logout(request):
    request.session.flush()
    return redirect('main:home')

#--- Register Pengguna Publik ---#
def register_pengguna_publik(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegPenggunaForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                print(email)
                password = request.POST.get('password')
                nama = request.POST.get('nama')
                nik = request.POST.get('nik')
                no_telepon = request.POST.get('no_telepon')

                 #Insert into AKUN_PENGGUNA
                cursor.execute("INSERT INTO AKUN_PENGGUNA (username,password,peran)"
                               "VALUES ('{}','{}','pengpub');".format(email,password))

                #Insert into PENGGUNA 
                cursor.execute("INSERT INTO PENGGUNA_PUBLIK(username, nama, nik, noHp, peran, status)"
                                "VALUES ('{}', '{}', '{}', '{}', 'pengpub', 'aktif');".format(email,nama,nik,no_telepon))


                request.session['username_account'] = email
                request.session["role"] = "pengpub"
                return redirect('user_profile:pengguna_publik_profile')
    form = RegPenggunaForm()
    args = {
        'form' : form,
    }
    return render(request, 'registerPengguna.html', args)

#--- Register Dokter ---#
def register_dokter(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegDokterForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                print(email)
                peran = 'dokter'
                password = request.POST.get('password')
                nostr = request.POST.get('nostr')
                nama = request.POST.get('nama')
                nohp = request.POST.get('nohp')
                gelardepan  = request.POST.get('gelardepan')
                gelarbelakang = request.POST.get('gelarbelakang')
                
                #Insert into AKUN_PENGGUNA
                cursor.execute("INSERT INTO AKUN_PENGGUNA (username,password,peran)"
                               "VALUES ('{}','{}','{}');".format(email,password,peran))
                
                 #Insert Into ADMIN

                cursor.execute("INSERT INTO ADMIN (username)"
                               "VALUES ('{}');".format(email))


                 #Insert into DOKTER
                cursor.execute("INSERT INTO DOKTER (nostr,username,nama,nohp,gelardepan,gelarbelakang)"
                               "VALUES ('{}','{}','{}','{}','{}','{}');".format(nostr, email, nama,nohp, gelardepan, gelarbelakang))


                request.session['username_account'] = email
                request.session["role"] = "dokter"
                return redirect('user_profile:dokter_profile')
    form = RegDokterForm()
    args = {
        'form' : form,
    }
    return render(request, 'registerDokter.html', args)


#------------REGISTRASI ADMIN SISTEM------------#
def register_admin_sistem(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegAdmSistemForm(request.POST) 
            if form.is_valid():
                email = request.POST.get('email')
                print(email)
                password = request.POST.get('password')
               
               #Insert into AKUN_PENGGUNA
                cursor.execute("INSERT INTO AKUN_PENGGUNA (username,password,peran)"
                               "VALUES ('{}','{}','admin sistem');".format(email,password))
               #Insert Into ADMIN
                cursor.execute("INSERT INTO ADMIN (username)"
                               "VALUES ('{}');".format(email))

                request.session['username_account'] = email
                request.session["role"] = "admin sistem"
                return redirect('user_profile:admin_sistem_profile')
    form = RegAdmSistemForm()
    args = {
        'form' : form,
    }
    return render(request, 'registerAdminSistem.html', args)

#------------REGISTRASI ADMIN SATGAS------------#
def register_admin_satgas(request):
    with connection.cursor() as cursor:
        response = {}
        response['error'] = False
        form = RegAdmSatgasForm(request.POST) 
        response['form'] = form
        if request.method == 'POST':
            if form.is_valid():
                email = request.POST.get('email')
                print(email)
                password = request.POST.get('password')
                kode_faskes = form.cleaned_data['kode_faskes']
               
               #Insert into AKUN_PENGGUNA
                cursor.execute("INSERT INTO AKUN_PENGGUNA (username,password,peran)"
                               "VALUES ('{}','{}','admin satgas');".format(email,password))
               
               #Insert Into ADMIN
                cursor.execute("INSERT INTO ADMIN (username)"
                               "VALUES ('{}');".format(email))

               #Insert Into ADMIN SATGAS
                cursor.execute("INSERT INTO ADMIN_SATGAS (username, IdFaskes)"
                               "VALUES ('{}','{}');".format(email,kode_faskes))

                request.session['username_account'] = email
                request.session["role"] = "admin satgas"
                request.session['kode_faskes'] = kode_faskes

                return redirect('user_profile:admin_satgas_profile')

    form = RegAdmSatgasForm()
    args = {
        'form' : form,
    }
    return render(request, 'registerAdminSatgas.html', args)

#---- GET ALL KODE_FASKES ----#

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

