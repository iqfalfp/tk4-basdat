from django import forms
from django.db import connection

class LoginForm(forms.Form):
    
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
        }))

class RegPenggunaForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
        }))

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
        }))

    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nama',
        'type' : 'text',
        'required': True,
        }))

    nik = forms.IntegerField(label='NIK', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'NIK',
        'type' : 'text',
        'required': True,
        }))

    no_telepon = forms.IntegerField(label='No. Telepon', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'No. Telepon',
        'type' : 'text',
        'required': True,
        }))

class RegAdmSistemForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
        }))

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
        }))

class RegAdmSatgasForm(forms.Form):

    def get_all_kode_faskes():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kode,kode FROM FASKES;")
        kode_response = cursor.fetchall()
        cursor.close()

        return kode_response



    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
        }))

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
        }))

    kode_faskes = forms.ChoiceField(label="Kode Faskes", choices= get_all_kode_faskes())

class RegDokterForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
        }))

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
        }))

    nostr = forms.CharField(label='No. STR', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'No. STR',
        'type' : 'text',
        'required': True,
        }))

    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nama',
        'type' : 'text',
        'required': True,
        }))

    nohp = forms.IntegerField(label='No. HP', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'No. HP',
        'type' : 'text',
        'required': True,
        }))

    gelardepan = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Gelar Depan',
        'type' : 'text',
        'required': True,
        }))

    gelarbelakang = forms.CharField(label='Gelar Belakang', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Gelar Belakang',
        'type' : 'text',
        'required': True,
        }))
