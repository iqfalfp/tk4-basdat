from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'users'

urlpatterns = [
    path('register/adminsistem', views.register_admin_sistem, name='register_admin_sistem'),
    path('register/adminsatgas', views.register_admin_satgas, name='register_admin_satgas'),
    path('register/dokter', views.register_dokter, name='register_dokter'),
    path('register/penggunapublik', views.register_pengguna_publik, name='register_pengguna_publik'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),

]
