from django.shortcuts import render

# Create your views here.
from django.http import response, JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreatePaketMakanan, UpdatePaketMakanan, CreateTransaksiMakan,UpdateTransaksiMakanan
import json

def fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]

def create_paket_makan(request):
    with connection.cursor() as cursor:
        response = {}
        response['error'] = False
        form = CreatePaketMakanan(request.POST or None)
        response['form'] = form
        if request.method == 'POST':
            if form.is_valid():
                kode_hotel = request.POST.get('kode_hotel')
                kode_paket = request.POST.get('kode_paket')
                nama_paket = request.POST.get('nama_paket')
                harga = request.POST.get('harga')

                    # Insert Into PAKET_MAKAN
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("INSERT INTO PAKET_MAKAN (kodehotel,kodepaket,nama,harga) VALUES ('{}','{}','{}','{}');".format(kode_hotel,kode_paket,nama_paket,harga))

                return redirect('transaksi_makanan:list_paket_makan')

    form =  CreatePaketMakanan()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : form,
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_paket_makan.html',args)

def list_paket_makan(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT KodeHotel, KodePaket, nama, harga FROM PAKET_MAKAN;')
    list_paket_makan = cursor.fetchall()
    cursor.close()

    cursor = connection.cursor()
    cursor.execute('SELECT * FROM PAKET_MAKAN WHERE kodepaket NOT IN (SELECT kodepaket FROM DAFTAR_PESAN WHERE DAFTAR_PESAN.kodehotel = PAKET_MAKAN.kodehotel AND DAFTAR_PESAN.kodepaket = PAKET_MAKAN.kodepaket);')
    belum_daftar_pesan = cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute("SELECT kodehotel, kodepaket, nama, harga "
                            "FROM PAKET_MAKAN")
        paket_makan_data = fetchall(cursor)
        kode_hotel = paket_makan_data[0]["kodehotel"]
        kode_paket =  paket_makan_data[0]["kodepaket"]
        nama =  paket_makan_data[0]["nama"]
        harga =  paket_makan_data[0]["harga"]

        can_delete = []
        for i in paket_makan_data:
            tmp = i['kodehotel']+i['kodepaket']
            is_can = False
            for j in belum_daftar_pesan:
                tmp2 = j[0]+j[1]
                if tmp == tmp2:
                    is_can = True
            if not is_can:
                can_delete.append(0)
            else:
                can_delete.append(1)

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

        args = {
            'list_paket_makan':list(zip(list_paket_makan,can_delete)),
            'kode_hotel':kode_hotel,
            'kode_paket':kode_paket,
            'nama':nama,
            'harga':harga,
            'username' : username,
            'password' : password,
            'peran' : peran,
        }
        return render(request, 'list_paket_makan.html', args)

def update_paket_makan(request,row1, row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT * FROM PAKET_MAKAN WHERE kodehotel ='{}' AND kodepaket ='{}';".format(row1,row2))
    data_paket_makan = cursor.fetchone()
    response = {}
    response['kode_hotel'] = data_paket_makan[0]
    response['kode_paket'] = data_paket_makan[1]
    response['nama_paket'] = data_paket_makan[2]
    response['harga'] = data_paket_makan[3]
    print(response)
    cursor.close()
    initial_dict = {
        'kode_hotel': response['kode_hotel'],
        'kode_paket': response['kode_paket'],
        'nama_paket': response['nama_paket'],
        'harga': response['harga'],
        }

    response['error'] = False
    form = UpdatePaketMakanan(request.POST or None, initial=initial_dict)
    response['form'] = form 

    if request.method == 'POST':
        if form.is_valid():
            kode_hotel = form.cleaned_data['kode_hotel']
            kode_paket = form.cleaned_data['kode_paket']
            nama_paket = request.POST.get('nama_paket')
            harga = request.POST.get('harga')
            
            # UPDATE PAKET MAKAN
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("UPDATE PAKET_MAKAN SET kodeHotel ='{}', kodePaket ='{}', nama ='{}', harga ='{}' WHERE kodeHotel ='{}' AND kodePaket ='{}';".format(kode_hotel,kode_paket,nama_paket,harga,kode_hotel,kode_paket))
                return redirect('transaksi_makanan:list_paket_makan')

    form = UpdatePaketMakanan()
    args = {
        'form' : response['form'],
    }

    return render(request, 'update_paket_makan.html', args) 

def delete_paket_makan(request,row1,row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("DELETE FROM PAKET_MAKAN WHERE kodehotel ='{}' AND kodepaket ='{}';".format(row1,row2))
    cursor.close()
    return redirect('transaksi_makanan:list_paket_makan') 

def create_transaksi_makan(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT idTransaksi FROM TRANSAKSI_HOTEL;")
        data_transaksi_hotel = list(map(lambda x:(x[0],x[0]), cursor.fetchall()))
        print(data_transaksi_hotel)

        cursor.execute("SELECT idTransaksiMakan FROM TRANSAKSI_MAKAN ORDER BY idTransaksiMakan desc LIMIT 1;")
        last_idtransaksimakan = cursor.fetchone()
        print(last_idtransaksimakan)

        id_makan = f'TRM{int(last_idtransaksimakan[0][3:]) + 1}'

    
    initialdict = {
        "idTransaksi" : data_transaksi_hotel, 
        "idTransaksi_Makan": id_makan,
        }

    response = {}
    response['error'] = False
    data = None
    if (request.method == 'POST'):
        data = json.loads(request.POST.get('data'))
    form = CreateTransaksiMakan(data or None, initial = initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            idTransaksi_Makan = form.cleaned_data['idTransaksi_Makan']
            idTransaksi =  form.cleaned_data['idTransaksi']

            #    Insert Into HOTEL
            with connection.cursor() as cursor:
                # INSERT TRANSAKSI MAKANAN
                cursor.execute("INSERT INTO TRANSAKSI_MAKAN (IdTransaksiMakan, IdTransaksi) VALUES ('{}','{}');".format(idTransaksi_Makan,idTransaksi))

                cursor.execute("SELECT id_Pesanan FROM DAFTAR_PESAN ORDER BY id_Pesanan desc LIMIT 1;")
                last_idpesanan = cursor.fetchone()[0]

                cursor.execute("SELECT KodeHotel FROM reservasi_hotel NATURAL JOIN transaksi_booking WHERE IdTransaksiBooking ='{}';".format(idTransaksi))
                kode_hotel = cursor.fetchone()[0]
                
                for kode_paket in data["listKodePaket"]:
                    # print(kode_paket)
                    cursor.execute("INSERT INTO DAFTAR_PESAN (IdTransaksiMakan, idtransaksi, id_pesanan,  KodeHotel, KodePaket ) VALUES ('{}','{}','{}','{}','{}');".format(idTransaksi_Makan,idTransaksi,last_idpesanan + 1,kode_hotel,kode_paket))
                    last_idpesanan += 1
            return JsonResponse({})

    form = CreateTransaksiMakan()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_transaksi_makan.html', args)

def create_transaksi_makan2(request):
    id_makan = ''
    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(*) FROM ID_KEEPER;")
        code_length = cursor.fetchone()
        print("CODE LENGTH")
        print(code_length[0])

    if code_length[0] == 0 :

        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM TRANSAKSI_MAKAN;")
            numbase = cursor.fetchone()
            cursor.close()
            print(numbase[0])
            numbase_new = numbase[0]+1
            numbase_str = str(numbase_new)
            if numbase_new < 10:
                id_makan = 'TRM0'+ numbase_str
            else :
                id_makan = 'TRM' + numbase_str
    else :
        with connection.cursor() as cursor:
            cursor.execute("SELECT code FROM ID_KEEPER LIMIT 1;")
            numbase = cursor.fetchone()
            print(numbase[0])
            code = numbase[0]

    initialdict = {"id_makan" : id_makan}

    response = {}
    response['error'] = False
    form = CreateTransaksiMakan(request.POST or None, initial = initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            idTransaksi_Makan = form.cleaned_data['idTransaksi_Makan']
            idTransaksi = request.POST.get('idTransaksi')
            kode_hotel = form.cleaned_data['kode_hotel'] #auto generated
            kode_paket = request.POST.get('kode_paket')

            #    Insert Into HOTEL
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")

                # INSERT TRANSAKSI MAKANAN
                cursor.execute("INSERT INTO TRANSAKSI_MAKAN (IdTransaksiMakan, IdTransaksi, TotalBayar) VALUES ('{}','{}','{}');".format(idTransaksi_Makan,idTransaksi,TotalBayar)) #Total bayar masih bingung dapet dari mana

                # INSERT DAFTAR PESAN
                cursor.execute("INSERT INTO DAFTAR_PESAN (IdTransaksiMakan, id_transaksi, id_pesanan,  KodeHotel, KodePaket ) VALUES ('{}','{}','{}','{}','{}');".format(idTransaksi_Makan,idTransaksi,id_pesanan,kode_hotel,kode_paket)) #Id pesanan masih bingung dapet dari mana
                return redirect('transaksi_makanan:list_transaksi_makan')

    form = CreateTransaksiMakan()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_transaksi_makan.html', args)

def list_transaksi_makan(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM TRANSAKSI_MAKAN;')
    list_transaksi_makan = cursor.fetchall()
    cursor.close()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

        args = {
            'list_transaksi_makan':list_transaksi_makan,
            'username' : username,
            'password' : password,
            'peran' : peran,
        }
        return render(request, 'list_transaksi_makan.html', args)

def delete_transaksi_makan(request,row1,row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("DELETE FROM TRANSAKSI_MAKAN WHERE IdTransaksiMakan ='{}' AND IdTransaksi='{}';".format(row1,row2))
    cursor.close()
    return redirect('transaksi_makanan:list_transaksi_makan') 

def update_transaksi_makan(request,row1,row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT * FROM TRANSAKSI_MAKAN WHERE IdTransaksiMakan ='{}' AND IdTransaksi='{}';".format(row1,row2))
    data_transaksi_makan = cursor.fetchone()
    response = {}
    response['idTransaksi_Makan'] = data_transaksi_makan[0]
    response['idTransaksi'] = data_transaksi_makan[1]
    response['kode_hotel'] = data_transaksi_makan[2]
    response['kode_paket'] = data_transaksi_makan[3]
    print(response)
    cursor.close()
    initial_dict = {
        'idTransaksi_Makan': response['idTransaksi_Makan'],
        'idTransaksi': response['idTransaksi'],
        'kode_hotel': response['kode_hotel'],
        'kode_paket': response['kode_paket'],
        }

    response['error'] = False
    form = UpdatePaketMakanan(request.POST or None, initial=initial_dict)
    response['form'] = form 

    if request.method == 'POST':
        if form.is_valid():
            idTransaksi_Makan = form.cleaned_data['idTransaksi_Makan']
            idTransaksi = form.cleaned_data['idTransaksi']
            kode_hotel = request.POST.get('kode_hotel')
            kode_paket = request.POST.get('kode_paket')
            
            # UPDATE PAKET MAKAN
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("UPDATE TRANSAKSI_MAKAN SET IdTransaksiMakan ='{}', IdTransaksi ='{}', TotalBayar ='{}';".format(kode_hotel,kode_paket,nama_paket,harga,kode_hotel,kode_paket))
                cursor.execute("UPDATE DAFTAR_PESAN SET IdTransaksiMakan ='{}', id_transaksi ='{}', id_pesanan ='{}', KodeHotel ='{}', KodePaket ='{}';".format(idTransaksi_Makan,idTransaksi,id_pesanan,kode_hotel,kode_paket))
                return redirect('transaksi_makanan:list_paket_makan')

    form = UpdatePaketMakanan()

    args = {
        'form' : response['form'],
    }

    return render(request, 'update_paket_makan.html', args) 


def detail_transaksi_makan(request,row1,row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT * FROM TRANSAKSI_MAKAN WHERE IdTransaksiMakan ='{}' AND IdTransaksi='{}';".format(row1,row2))
    data_transaksi_makan = cursor.fetchone()
    response = {}
    response['idTransaksi_Makan'] = data_transaksi_makan[0]
    response['idTransaksi'] = data_transaksi_makan[1]
    response['kode_hotel'] = data_transaksi_makan[2]
    response['kode_paket'] = data_transaksi_makan[3]
    cursor.close()

    return render(request, 'detail_transaksi_makan.html', response)

def get_kode_hotel(request):
    #print(request.__dict__)
    id_transaksi = request.GET.get("idTransaksi")
    with connection.cursor() as cursor:
        cursor.execute("SELECT KodeHotel FROM reservasi_hotel NATURAL JOIN transaksi_booking WHERE IdTransaksiBooking ='{}';".format(id_transaksi))
        kode_hotel = cursor.fetchone()[0]
        cursor.execute("SELECT KodePaket from paket_makan WHERE KodeHotel='{}'".format(kode_hotel))
        data_kode_paket = list(map(lambda x:x[0], cursor.fetchall()))

    return JsonResponse({"kodeHotel": kode_hotel, "paketMakanList":data_kode_paket}) 
     












                    

