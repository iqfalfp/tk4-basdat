from django import forms
from django.db import connection

class CreatePaketMakanan(forms.Form):
    def get_all_kode_hotel():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kode FROM HOTEL;")
        kode_response = cursor.fetchall()
        res = []
        for tup in kode_response:
            res.append((tup[0], tup[0]))
        cursor.close()

        return res


    # Kode Hotel: H01 [Dropdown, Ambil data dari tabel HOTEL]
    kode_hotel = forms.ChoiceField(label="Kode Hotel", choices= get_all_kode_hotel())

    # Kode Paket: MACEH [Isian]
    kode_paket = forms.CharField(label='Kode Paket', max_length=5, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis kode paket',
        'type' : 'text',
        'required': True,
    }))

    # Nama Paket: Mie Aceh dengan Bumbu Spesial “Minyak Babi Panggang”.
    nama_paket = forms.CharField(label='Nama Paket', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis kode paket',
        'type' : 'text',
        'required': True,
    }))
    
    # Harga: Rp70.000 [Isian]
    harga = forms.IntegerField(label='Harga',required=True,widget=forms.NumberInput,min_value=0)

class UpdatePaketMakanan(forms.Form):
    kode_hotel = forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    kode_paket =  forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    nama_paket = forms.CharField(label='Nama Paket', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis kode paket',
        'type' : 'text',
        'required': True,
    }))

    harga = forms.IntegerField(label='Harga',required=False,widget=forms.NumberInput)

class CreateTransaksiMakan(forms.Form):
    def get_data_transaksi_hotel():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select idtransaksi  FROM TRANSAKSI_HOTEL;")
        transaksi_hotel = cursor.fetchall()
        res = []
        for tup in transaksi_hotel:
            res.append((tup[0], tup[0]))
        cursor.close()

        return res
 
    
    def get_all_paket_makan():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kodepaket FROM PAKET_MAKAN;")
        paket_makan = cursor.fetchall()
        res = []
        for tup in paket_makan:
            res.append((tup[0], tup[0]))
        cursor.close()

        return res
        
    idTransaksi_Makan = forms.CharField(label='Id Transaksi Makan', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    idTransaksi = forms.ChoiceField(label="Id Transaksi", choices= get_data_transaksi_hotel())

class UpdateTransaksiMakanan(forms.Form):
    def get_all_paket_makan():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kodepaket FROM PAKET_MAKAN;")
        paket_makan = cursor.fetchall()
        res = []
        for tup in paket_makan:
            res.append((tup[0], tup[0]))
        cursor.close()

        return res

    idTransaksi_Makan =  forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    idTransaksi = forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))


    kode_hotel =  forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    kode_paket = forms.ChoiceField(label="Kode Paket", choices= get_all_paket_makan())

