from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'transaksi_makanan'

urlpatterns = [
    path('transaksi_makan/create_paket_makan', views.create_paket_makan, name='create_paket_makan'),
    path('transaksi_makan/list_paket_makan', views.list_paket_makan, name='list_paket_makan'),
    path('transaksi_makan/update_paket_makan/<str:row1>/<str:row2>', views.update_paket_makan, name='update_paket_makan'),
    path('transaksi_makan/delete_paket_makan/<str:row1>/<str:row2>', views.delete_paket_makan, name='delete_paket_makan'),
    path('transaksi_makan/create_transaksi_makan', views.create_transaksi_makan, name='create_transaksi_makan'),
    path('transaksi_makan/list_transaksi_makan', views.list_transaksi_makan, name='list_transaksi_makan'),
    path('transaksi_makan/delete_transaksi_makan/<str:row1>/<str:row2>', views.delete_transaksi_makan, name='delete_transaksi_makan'),
    path('transaksi_makan/update_transaksi_makan/<str:row1>/<str:row2>', views.update_transaksi_makan, name='update_transaksi_makan'),
    path('transaksi_makan/detail_transaksi_makan/<str:row1>/<str:row2>', views.detail_transaksi_makan, name='detail_transaksi_makan'),
    path('transaksi_makan/get_kode_hotel', views.get_kode_hotel, name='get_kode_hotel'),
]
