from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name="home"),
]