from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.db import connection

# Create your views here.
def fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def pengguna_publik_profile(request):
    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, nik, nama, status, peran, nohp "
                        "FROM PENGGUNA_PUBLIK WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        nik = profile_data[0]["nik"]
        nama = profile_data[0]["nama"]
        nohp = profile_data[0]["nohp"]
        args = {
            'username' : username,
            'nik' : nik,
            'nama' : nama,
            'nohp' : nohp,
        }
        print(profile_data)
        return render(request, 'profil_pengguna_publik.html', args) 

def dokter_profile(request):
    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT nostr, username, nama, nohp, gelardepan, gelarbelakang "
                        "FROM DOKTER WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        nostr = profile_data[0]["nostr"]
        nama = profile_data[0]["nama"]
        nohp = profile_data[0]["nohp"]
        gelar_depan = profile_data[0]["gelardepan"]
        gelar_belakang = profile_data[0]["gelarbelakang"]
        args = {
            'username' : username,
            'nostr' : nostr,
            'nama' : nama,
            'nohp' : nohp,
            'gelar_depan' : gelar_depan,
            'gelar_belakang' : gelar_belakang
        }
        return render(request, 'profil_dokter.html', args) 

def admin_sistem_profile(request):
    email = request.session.get("username_account")
    profile_data = email
    args = {
        'profile_data' : profile_data,
    }
    return render(request, 'profil_admin_sistem.html', args) 

def admin_satgas_profile(request):
    email = request.session.get("username_account") 
    print(email)  
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, IdFaskes"
                        " FROM ADMIN_SATGAS WHERE username = '{}';".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        idfaskes = profile_data[0]["idfaskes"]
        args = {
            'username' : username,
            'idfaskes' : idfaskes,
            }
        return render(request, 'profil_admin_satgas.html', args)

def profil_view(request):
    peran = request.session.get("peran")
    if peran == None:
        return HttpResponse("Please Login")
    if peran == "admin satgas":
        return redirect('user_profile:admin_satgas_profile')
    elif peran == "admin sistem":
        return redirect('user_profile:admin_sistem_profile')
    elif peran == "pengpub":
        return redirect('user_profile:pengguna_publik_profile')
    elif peran == "dokter":
        return redirect('user_profile:dokter_profile')
