from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'user_profile'

urlpatterns = [
    path('user_profile/adminsatgas', views.admin_satgas_profile, name='admin_satgas_profile'),
    path('user_profile/adminsistem', views.admin_sistem_profile, name='admin_sistem_profile'),
    path('user_profile/dokter', views.dokter_profile, name='dokter_profile'),
    path('user_profile/penggunapublik', views.pengguna_publik_profile, name='pengguna_publik_profile'),
]