from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'hotel'

urlpatterns = [
    path('hotel/create_hotel', views.form_create_hotel, name='create_hotel'),
    path('hotel/list_hotel', views.list_hotel, name='list_hotel'),
    path('hotel/update_hotel/<str:row>', views.form_update_hotel, name='update_hotel'),
]
