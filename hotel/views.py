from django.shortcuts import render

# Create your views here.
from django.http import response
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateHotel, UpdateHotel

def fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]


def form_create_hotel(request):
    kode_hotel = ''
    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(*) FROM HOTEL;")
        numbase = cursor.fetchone()
        cursor.close()
        print(numbase[0])
        numbase_new = numbase[0]+1
        numbase_str = str(numbase_new)
        if numbase_new < 10:
            kode_hotel = 'H0'+ numbase_str
        else :
            kode_hotel = 'H' + numbase_str

    initialdict = {"kode_hotel" : kode_hotel}
    response = {}
    form = CreateHotel(request.POST or None, initial= initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            print("form valid")
            kode_hotel = form.cleaned_data['kode_hotel']
            nama_hotel = request.POST.get('nama_hotel')
            rujukan = form.cleaned_data['rujukan']
            if rujukan == True :
                rujukan = 1
            if rujukan == False :
                rujukan =  0
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')

            #    Insert Into HOTEL
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("INSERT INTO HOTEL (Kode, nama, isRujukan, jalan, kelurahan, kecamatan, kabkot, prov) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}');".format(kode_hotel,nama_hotel,rujukan,jalan,kelurahan,kecamatan,kabkot,provinsi))
                print("anjinggg")
                return redirect('hotel:list_hotel')
    
    form = CreateHotel()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_hotel.html', args)
    
def list_hotel(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kode, nama, isRujukan, jalan, kelurahan, kecamatan, kabkot, prov FROM HOTEL ORDER BY KODE;')
    list_hotel = cursor.fetchall()
    print(list_hotel)
    print(len(list_hotel))

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'list_hotel':list_hotel,
        'username' : username,
        'password' : password,
        'peran' : peran,
        }
    return render(request, 'list_hotel.html', args)

def form_update_hotel(request,row):
    print(row)
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM HOTEL WHERE kode = %s;',[row])
    data_hotel = cursor.fetchone()
    response = {}
    response['kode'] = data_hotel[0]
    response['nama'] = data_hotel[1]
    response['isRujukan'] = data_hotel[2]
    response['jalan'] = data_hotel[3]
    response['kelurahan'] = data_hotel[4]
    response['kecamatan'] = data_hotel[5]
    response['kabkot'] = data_hotel[6]
    response['prov'] = data_hotel[7]
    print(response)
    cursor.close()
    initial_dict = {
        "kode_hotel":response['kode'], 
        "nama_hotel":response['nama'], 
        "isRujukan":response['isRujukan'], 
        "jalan":response['jalan'], 
        "kelurahan":response['kelurahan'],
        "kecamatan":response['kecamatan'],
        "kabkot":response['kabkot'], 
        "provinsi":response['prov']
        }

    response['error'] = False
    form = UpdateHotel(request.POST or None, initial=initial_dict)
    response['form'] = form 

    if request.method == 'POST':
        if form.is_valid():
            kode_hotel = form.cleaned_data['kode_hotel']
            nama_hotel = request.POST.get('nama_hotel')
            rujukan = form.cleaned_data['rujukan']
            if rujukan == True :
                rujukan = 1
            if rujukan == False :
                rujukan =  0
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')

            # UPDATE HOTEL
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("UPDATE HOTEL SET kode ='{}', nama ='{}', isRujukan ='{}', jalan ='{}', kelurahan ='{}', kecamatan ='{}', kabkot ='{}', prov ='{}' WHERE kode ='{}';".format(kode_hotel,nama_hotel,rujukan,jalan,kelurahan,kecamatan,kabkot,provinsi,kode_hotel))
                return redirect('hotel:list_hotel')

    form = UpdateHotel()
    args = {
        'form' : response['form'],
    }

    return render(request, 'update_hotel.html', args) 




