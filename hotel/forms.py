from django import forms
from django.db import connection

class CreateHotel(forms.Form):
    kode_hotel = forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    #nama hotel : isian
    nama_hotel = forms.CharField(label='Nama Hotel', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama hotel',
        'type' : 'text',
        'required': True,
    }))

    #Rujukan: ☑ [Checkbox bisa diklik]
    rujukan = forms.BooleanField(label='rujukan', required=False)

    #Alamat:
    # Jalan: [Isian]
    jalan = forms.CharField(label='Jalan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama jalan',
        'type' : 'text',
        'required': True,
    }))
    #  Kelurahan: [Isian]
    kelurahan = forms.CharField(label='Kelurahan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kelurahan',
        'type' : 'text',
        'required': True,
    }))
    #  Kecamatan: [Isian]
    kecamatan = forms.CharField(label='kecamatan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kecamatan',
        'type' : 'text',
        'required': True,
    }))
    #  Kabupaten/Kota: [Isian]
    kabkot = forms.CharField(label='Kabupaten/Kota', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kabupaten atau kota',
        'type' : 'text',
        'required': True,
    }))
    #  Provinsi: [Isian]
    provinsi = forms.CharField(label='Provinsi', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama provinsi',
        'type' : 'text',
        'required': True,
    }))

class UpdateHotel(forms.Form):
    kode_hotel = forms.CharField(label='Kode Hotel', max_length=10, disabled = True, widget = forms.TextInput(attrs={
        'readonly':'readonly'
    }))

    nama_hotel = forms.CharField(label='Nama Hotel', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama hotel',
        'type' : 'text',
        'required': True,
    }))

    #Rujukan: ☑ [Checkbox bisa diklik]
    rujukan = forms.BooleanField(label='rujukan',required=False)

    #Alamat:
    # Jalan: [Isian]
    jalan = forms.CharField(label='Jalan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama jalan',
        'type' : 'text',
        'required': True,
    }))
    #  Kelurahan: [Isian]
    kelurahan = forms.CharField(label='Kelurahan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kelurahan',
        'type' : 'text',
        'required': True,
    }))
    #  Kecamatan: [Isian]
    kecamatan = forms.CharField(label='kecamatan', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kecamatan',
        'type' : 'text',
        'required': True,
    }))
    #  Kabupaten/Kota: [Isian]
    kabkot = forms.CharField(label='Kabupaten/Kota', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama kabupaten atau kota',
        'type' : 'text',
        'required': True,
    }))
    #  Provinsi: [Isian]
    provinsi = forms.CharField(label='Provinsi', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis nama provinsi',
        'type' : 'text',
        'required': True,
    }))