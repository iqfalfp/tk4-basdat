from django.http import response
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateFaskes, UpdateFaskes,CreateJadwal

# Create your views here.

def fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]
    
def list_faskes(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kode, nama, tipe FROM FASKES;')
    list_faskes = cursor.fetchall()
    print(list_faskes)
    print(len(list_faskes))

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        cursor.execute("SELECT idfaskes FROM ADMIN_SATGAS WHERE username = '{}';".format(email))
        akun_data = fetchall(cursor)
        print(profile_data)
        print(akun_data)
        print(akun_data[0]['idfaskes'])
        id_faskes = akun_data[0]['idfaskes']
        print(id_faskes)

    args = {'list_faskes':list_faskes,
            'username' : username,
            'password' : password,
            'peran' : peran,
            'id_faskes' : id_faskes}
    return render(request, 'list_faskes.html', args)

def detail_faskes(request, row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM FASKES WHERE kode = %s;',[row])
    data_faskes = cursor.fetchone()
    response = {}
    response['kode'] = data_faskes[0]
    response['tipe'] = data_faskes[1]
    response['nama'] = data_faskes[2]
    response['status_kepemilikan'] = data_faskes[3]
    response['jalan'] = data_faskes[4]
    response['kelurahan'] = data_faskes[5]
    response['kecamatan'] = data_faskes[6]
    response['kabkot'] = data_faskes[7]
    response['provinsi'] = data_faskes[8]
    print(response)
    cursor.close()

    return render(request, 'detail_faskes.html', response)

def form_create_faskes(request):
    code = ''
    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(*) FROM CODE_KEEPER;")
        code_length = cursor.fetchone()
        print("CODE LENGTH")
        print(code_length[0])

    if code_length[0] == 0 :

        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM FASKES;")
            numbase = cursor.fetchone()
            cursor.close()
            print(numbase[0])
            numbase_new = numbase[0]+1
            numbase_str = str(numbase_new)
            if numbase_new < 10:
                code = 'F0'+ numbase_str
            else :
                code = 'F' + numbase_str

    else :

        with connection.cursor() as cursor:
            cursor.execute("SELECT code FROM CODE_KEEPER LIMIT 1;")
            numbase = cursor.fetchone()
            print(numbase[0])
            code = numbase[0]


    initialdict = {"kode" : code}

    print(initialdict)
    response = {}
    response['error'] = False
    form = CreateFaskes(request.POST or None, initial = initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode_faskes = form.cleaned_data['kode']
            tipe = request.POST.get('tipe')
            nama_faskes = request.POST.get('nama_faskes')
            status_kepemilikan = request.POST.get('status_kepemilikan')
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')

            #    Insert Into FASKES
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("INSERT INTO FASKES VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}');".format(kode_faskes,tipe,nama_faskes,status_kepemilikan,jalan,kelurahan,kecamatan,kabkot,provinsi))
                print('test')

                cursor.execute("SELECT COUNT(*) FROM CODE_KEEPER;")
                code_length = cursor.fetchone()
                print("CODE LENGTH")
                print(code_length[0])

                if code_length != 0:
                    cursor.execute("DELETE FROM CODE_KEEPER WHERE code = '{}';".format(code))


                return redirect('faskes:list_faskes') 

    form = CreateFaskes()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_faskes.html', args)

def form_update_faskes(request, row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM FASKES WHERE kode = %s;',[row])
    data_faskes = cursor.fetchone()
    response = {}
    response['kode'] = data_faskes[0]
    response['tipe'] = data_faskes[1]
    response['nama'] = data_faskes[2]
    response['status_kepemilikan'] = data_faskes[3]
    response['jalan'] = data_faskes[4]
    response['kelurahan'] = data_faskes[5]
    response['kecamatan'] = data_faskes[6]
    response['kabkot'] = data_faskes[7]
    response['provinsi'] = data_faskes[8]
    print(response)
    cursor.close()
    initial_dict = {"kode":response['kode'], "tipe":response['tipe'], "nama_faskes":response['nama'], "status_kepemilikan":response['status_kepemilikan'], "jalan":response['jalan'], "kelurahan":response['kelurahan'],"kecamatan":response['kecamatan'],"kabkot":response['kabkot'], "provinsi":response['provinsi']}

    response['error'] = False
    form = UpdateFaskes(request.POST or None, initial=initial_dict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode_faskes = form.cleaned_data['kode']
            tipe = request.POST.get('tipe')
            nama_faskes = request.POST.get('nama_faskes')
            status_kepemilikan = request.POST.get('status_kepemilikan')
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')

            # UPDATE FASKES
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("UPDATE FASKES SET kode ='{}', tipe = '{}', nama = '{}', statusmilik = '{}', jalan = '{}', kelurahan = '{}', kecamatan = '{}', kabkot = '{}', prov = '{}' WHERE kode = '{}';".format(kode_faskes,tipe,nama_faskes,status_kepemilikan,jalan,kelurahan,kecamatan,kabkot,provinsi,kode_faskes))
                print('test')
                return redirect('faskes:list_faskes') 
    
    form = UpdateFaskes()
    args = {
        'form' : response['form'],
    } 

    return render(request, 'update_faskes.html', args)

def delete_faskes(request,row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("INSERT INTO CODE_KEEPER VALUES('{}');".format(row))
    cursor.execute('DELETE FROM FASKES WHERE kode = %s;',[row])
    cursor.close()
    
    return redirect('faskes:list_faskes') 

def form_create_jadwal(request):
    response = {}
    response['error'] = False
    form = CreateJadwal(request.POST or None) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode = request.POST.get('kode')
            shift = request.POST.get('shift')
            tanggal = request.POST.get('tanggal')

            #    Insert Into JADWAL
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("INSERT INTO JADWAL VALUES ('{}','{}','{}');".format(kode,shift,tanggal))
                print('test_jadwal')
                return redirect('faskes:list_jadwal') 

    form = CreateJadwal()

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_jadwal.html', args)

def list_jadwal(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kode_faskes, shift, tanggal FROM JADWAL;')
    list_jadwal = cursor.fetchall()
    print(list_jadwal)
    print(len(list_jadwal))

    email = request.session.get("username_account")
    print(email)
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]
        print(profile_data)

    args = {'list_jadwal':list_jadwal,
            'username' : username,
            'password' : password,
            'peran' : peran}
    return render(request, 'list_jadwal.html', args)