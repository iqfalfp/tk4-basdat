from django import forms
from django.db import connection
import datetime



class CreateFaskes(forms.Form):
    # with connection.cursor() as cursor:
    #     cursor.execute("SELECT COUNT(*) FROM FASKES;")
    #     numbase = cursor.fetchone()
    #     cursor.close()
    #     print(numbase[0])
    #     numbase_new = numbase[0]+1
    #     numbase_str = str(numbase_new)
    #     if numbase_new < 10:
    #         code = 'F0'+ numbase_str
    #     else :
    #         code = 'F' + numbase_str

    kode =  forms.CharField(label='kode', disabled=True, required=True)

    tipe = forms.ChoiceField(label="Tipe", widget = forms.Select(), choices = ([('Rumah Sakit','Rumah Sakit'),('Puskesmas','Puskesmas'),('Klinik','Klinik')]), required = True)
    
    nama_faskes = forms.CharField(label='Nama Faskes', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nama Faskes',
        'type' : 'text',
        'required': True,
        }))
    
    status_kepemilikan = forms.ChoiceField(label="Status Kepemilikan", widget = forms.Select(), choices = ([('Swasta','Swasta'),('Pemerintah','Pemerintah')]), required = True)
    
    jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
        }))
    kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
        }))
    kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
        }))
    kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
        }))
    provinsi = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Provinsi',
        'type' : 'text',
        'required': True,
        }))

class UpdateFaskes(forms.Form):    

    kode =  forms.CharField(label='kode', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'kode',
        'type' : 'text',
        'required': False,
        'disabled': True,
        }))
        
    choice_tipe = [(1,'Rumah Sakit'),(2,'Puskesmas'),(3,'Klinik')]
    ownership = [(1,'Swasta'),(2,'Pemerintah')]

    tipe = forms.ChoiceField(label="Tipe", choices= choice_tipe)
    nama_faskes = forms.CharField(label='Nama Faskes', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nama Faskes',
        'type' : 'text',
        'required': True,
        }))
    status_kepemilikan = forms.ChoiceField(label="Status Kepemilikan", choices= ownership)
    jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
        }))
    kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
        }))
    kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
        }))
    kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
        }))
    provinsi = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Provinsi',
        'type' : 'text',
        'required': True,
        }))

class CreateJadwal(forms.Form):   

    def get_all_kode_faskes():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kode,Nama FROM FASKES;")
        kode_response = cursor.fetchall()
        cursor.close()

        return kode_response


    kode = forms.ChoiceField(label="Kode Faskes", choices= get_all_kode_faskes(), required=True)

    shift = forms.CharField(label='Shift Faskes', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Shift Faskes',
        'type' : 'text',
        'required': True,
        }))
   
    tanggal = forms.DateField(label ='tanggal',initial=datetime.date.today,required=True)

class UpdateFaskes(forms.Form):
    # with connection.cursor() as cursor:
    #     cursor.execute("SELECT COUNT(*) FROM FASKES;")
    #     numbase = cursor.fetchone()
    #     cursor.close()
    #     print(numbase[0])
    #     numbase_new = numbase[0]+1
    #     numbase_str = str(numbase_new)
    #     if numbase_new < 10:
    #         code = 'F0'+ numbase_str
    #     else :
    #         code = 'F' + numbase_str

    kode =  forms.CharField(label='kode', disabled=True, required=True)

    tipe = forms.ChoiceField(label="Tipe", widget = forms.Select(), choices = ([('Rumah Sakit','Rumah Sakit'),('Puskesmas','Puskesmas'),('Klinik','Klinik')]), required = True)
    
    nama_faskes = forms.CharField(label='Nama Faskes', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))
    
    status_kepemilikan = forms.ChoiceField(label="Status Kepemilikan", widget = forms.Select(), choices = ([('Swasta','Swasta'),('Pemerintah','Pemerintah')]), required = True)
    
    jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))
    kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))
    kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))
    kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))
    provinsi = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'class':'form-control',
        'type' : 'text',
        'required': True,
        }))

    