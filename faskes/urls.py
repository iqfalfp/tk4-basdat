from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'faskes'

urlpatterns = [
    # path('rumahsakit/', views.create_rs, name='create_rs'),
    path('faskes/create_faskes', views.form_create_faskes, name='create_form_faskes'),
    path('faskes/list_faskes', views.list_faskes, name='list_faskes'),
    path('faskes/detail_faskes/<str:row>', views.detail_faskes, name='detail_faskes'),
    path('faskes/update_faskes/<str:row>/', views.form_update_faskes, name='update_faskes'),
    path('faskes/delete_faskes/<str:row>', views.delete_faskes, name='delete_faskes'),
    path('faskes/create_jadwal', views.form_create_jadwal, name='create_form_jadwal'),
    path('faskes/list_jadwal', views.list_jadwal, name='list_jadwal')
]