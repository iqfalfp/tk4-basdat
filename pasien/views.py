from django.http import response
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreatePasien, UpdatePasien

def fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]

def form_create_pasien(request):

    email = request.session['username_account']
    print("ini create pasien")
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]

    
    initial_dict={}


    initial_dict = {
        'pendaftar' : email,
    }
    
    response = {}
    response['error'] = False
    form = CreatePasien(request.POST or None, initial= initial_dict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            pendaftar = email
            nik = request.POST.get('nik')
            nama_pasien = request.POST.get('nama_pasien')
            no_telp = request.POST.get('no_telp')
            no_hp = request.POST.get('no_hp')
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')
            jalan_dom = request.POST.get('jalan_dom')
            kelurahan_dom = request.POST.get('kelurahan_dom')
            kecamatan_dom = request.POST.get('kecamatan_dom')
            kabkot_dom = request.POST.get('kabkot_dom')
            provinsi_dom = request.POST.get('provinsi_dom')

            with connection.cursor() as cursor:
            #    Insert Into PASIEN
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("INSERT INTO PASIEN VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');".format(nik,pendaftar,nama_pasien,jalan,kelurahan,kecamatan,kabkot,provinsi,jalan_dom,kelurahan_dom,kecamatan_dom,kabkot_dom,provinsi_dom,no_telp,no_hp))

                return redirect('pasien:list_pasien') 


    form = CreatePasien()
    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_pasien.html', args)

def list_pasien(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT nik, nama FROM PASIEN;')
    list_pasien = cursor.fetchall()

    email = request.session.get("username_account")

    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password, peran "
                        "FROM AKUN_PENGGUNA WHERE username = '{}'".format(email))
        profile_data = fetchall(cursor)
        username = profile_data[0]["username"]
        password = profile_data[0]["password"]
        peran = profile_data[0]["peran"]

    args = {
        'list_pasien':list_pasien,
        'username' : username,
        'password' : password,
        'peran' : peran
    }   

    return render(request, 'list_pasien.html', args)

def detail_pasien(request, row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM PASIEN WHERE NIK = %s;',[row])
    data_pasien = cursor.fetchone()
    response = {}
    response['NIK'] = data_pasien[0]
    response['pendaftar'] = data_pasien[1]
    response['nama'] = data_pasien[2]
    response['jalan'] = data_pasien[3]
    response['kelurahan'] = data_pasien[4]
    response['kecamatan'] = data_pasien[5]
    response['kabkot'] = data_pasien[6]
    response['provinsi'] = data_pasien[7]
    response['jalan_dom'] = data_pasien[8]
    response['kelurahan_dom'] = data_pasien[9]
    response['kecamatan_dom'] = data_pasien[10]
    response['kabkot_dom'] = data_pasien[11]
    response['provinsi_dom'] = data_pasien[12]
    response['no_telepon'] = data_pasien[13]
    response['no_hp'] = data_pasien[14]
    cursor.close()

    return render(request, 'detail_pasien.html', response)
    
def form_update_pasien(request, row):

    email = request.session['username_account']
    print(email)
    with connection.cursor() as cursor:
        cursor = connection.cursor()
        cursor.execute('SET SEARCH_PATH TO SIRUCO;')
        cursor.execute('SELECT * FROM PASIEN WHERE NIK = %s;',[row])
        data_pasien = cursor.fetchone()
        responses = {}
        responses['NIK'] = data_pasien[0]
        responses['pendaftar'] = data_pasien[1]
        responses['nama'] = data_pasien[2]
        responses['jalan'] = data_pasien[3]
        responses['kelurahan'] = data_pasien[4]
        responses['kecamatan'] = data_pasien[5]
        responses['kabkot'] = data_pasien[6]
        responses['provinsi'] = data_pasien[7]
        responses['jalan_dom'] = data_pasien[8]
        responses['kelurahan_dom'] = data_pasien[9]
        responses['kecamatan_dom'] = data_pasien[10]
        responses['kabkot_dom'] = data_pasien[11]
        responses['provinsi_dom'] = data_pasien[12]
        responses['no_telepon'] = data_pasien[13]
        responses['no_hp'] = data_pasien[14]
        initialdict = {"nik" : data_pasien[0], 'pendaftar' : data_pasien[1], 'nama_pasien' : data_pasien[2],'jalan' : data_pasien[3], 'kelurahan' : data_pasien[4], 'kecamatan' : data_pasien[5] , 'kabkot' : data_pasien[6] , 'provinsi' : data_pasien[7], 'jalan_dom' : data_pasien[8] , 'kelurahan_dom' : data_pasien[9] ,'kecamatan_dom' : data_pasien[10], 'kabkot_dom' : data_pasien[11], 'provinsi_dom' : data_pasien[12], 'no_telp' : data_pasien[13], 'no_hp' : data_pasien[14]}
        print(initialdict)

    response = {}
    response['error'] = False
    form = UpdatePasien(request.POST or None, initial=initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            pendaftar = email
            nik = request.POST.get('nik')
            nama_pasien = request.POST.get('nama_pasien')
            no_telp = request.POST.get('no_telp')
            no_hp = request.POST.get('no_hp')
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')
            jalan_dom = request.POST.get('jalan_dom')
            kelurahan_dom = request.POST.get('kelurahan_dom')
            kecamatan_dom = request.POST.get('kecamatan_dom')
            kabkot_dom = request.POST.get('kabkot_dom')
            provinsi_dom = request.POST.get('provinsi_dom')
            print("sebelum cursor query")

            with connection.cursor() as cursor:
            #   Update Pasien
                print("A")
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                print("B")
                cursor.execute("UPDATE PASIEN SET nik = '{}', idpendaftar = '{}', nama = '{}', ktp_jalan = '{}', ktp_kelurahan = '{}', ktp_kecamatan = '{}', ktp_kabkot = '{}', ktp_prov = '{}', dom_jalan = '{}', dom_kelurahan = '{}', dom_kecamatan = '{}', dom_kabkot = '{}', dom_prov = '{}', notelp = '{}', nohp = '{}' WHERE nik = '{}';".format(data_pasien[0], data_pasien[1],data_pasien[2],jalan,kelurahan,kecamatan,kabkot,provinsi,jalan_dom,kelurahan_dom,kecamatan_dom,kabkot_dom,provinsi_dom,no_telp,no_hp,data_pasien[0]))
                print("sampai akhir")
                return redirect('pasien:list_pasien') 


    form = UpdatePasien()
    args = {
        'form' : response['form'],
    }
    return render(request, 'update_pasien.html', args)

def delete_pasien(request,row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('DELETE FROM PASIEN WHERE nik = %s;',[row])
    cursor.close()
    
    return redirect('pasien:list_pasien') 

