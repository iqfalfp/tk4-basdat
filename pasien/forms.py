from django import forms
from django.db import connection
from django.http import request, response, HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings

class CreatePasien(forms.Form):

    pendaftar =  forms.CharField(label='pendaftar', required=True, disabled=True)
        
    nik =  forms.IntegerField(label='nik', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'NIK',
        'type' : 'text',
        'required': True,
        }))

    nama_pasien = forms.CharField(label='Nama Pasien', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nama Pasien',
        'type' : 'text',
        'required': True,
        }))

    no_telp =  forms.IntegerField(label='Nomor Telepon', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nomor Telepon',
        'type' : 'text',
        'required': True,
        }))
    
    no_hp =  forms.IntegerField(label='Nomor HP', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Nomor HP',
        'type' : 'text',
        'required': True,
        }))

    jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
        }))

    kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
        }))

    kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
        }))

    kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
        }))

    provinsi = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Provinsi',
        'type' : 'text',
        'required': True,
        }))
    
    jalan_dom = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
        }))

    kelurahan_dom = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
        }))

    kecamatan_dom = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
        }))

    kabkot_dom = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
        }))
        
    provinsi_dom = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder': 'Provinsi',
        'type' : 'text',
        'required': True,
        }))

class UpdatePasien(forms.Form):

    pendaftar =  forms.CharField(label='pendaftar', required=True, disabled=True)
        
    nik =  forms.IntegerField(label='nik', required=True, disabled=True)

    nama_pasien = forms.CharField(label='Nama Pasien', required=True, disabled=True)

    no_telp =  forms.IntegerField(label='Nomor Telepon', required=True)
    
    no_hp =  forms.IntegerField(label='Nomor HP', required=True)

    jalan = forms.CharField(label='Jalan', required=True)

    kelurahan = forms.CharField(label='Kelurahan', required=True)

    kecamatan = forms.CharField(label='Kecamatan', required=True)

    kabkot = forms.CharField(label='Kabupaten/Kota', required=True)

    provinsi = forms.CharField(label='Provinsi', required=True)
    
    jalan_dom = forms.CharField(label='Jalan', required=True)

    kelurahan_dom = forms.CharField(label='Kelurahan', required=True)

    kecamatan_dom = forms.CharField(label='Kecamatan', required=True)

    kabkot_dom = forms.CharField(label='Kabupaten/Kota', required=True)
        
    provinsi_dom = forms.CharField(label='Provinsi', required=True)

class CreateAppointment(forms.Form):

    NIK_Pasien = ''

    emailDokter = ''
    
    faskes = ''

    tanggalPraktek = ''

    shift = ''
    