from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'pasien'

urlpatterns = [
    path('pasien/create_pasien', views.form_create_pasien, name='create_pasien'),
    path('pasien/list_pasien', views.list_pasien, name='list_pasien'),
    path('pasien/detail_pasien/<str:row>', views.detail_pasien, name='detail_pasien'),
    path('pasien/update_pasien/<str:row>', views.form_update_pasien, name='update_pasien'),
    path('pasien/delete_pasien/<str:row>', views.delete_pasien, name='delete_pasien')

]