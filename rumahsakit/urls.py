from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'rumahsakit'




urlpatterns = [
    path('rumahsakit/create_reservasi', views.form_create_reservasi_rs, name='create_form_reservasi_rs'),
    path('rumahsakit/create_rs', views.form_create_rs, name='create_form_rs'),
    path('rumahsakit/list_rs', views.list_rs, name='list_rs'),
    path('rumahsakit/update_rs/<str:row>', views.update_rs, name='update_rs'),
    path('rumahsakit/list_ruangan', views.get_ruangan, name='list_ruang'),
    path('rumahsakit/list_bed', views.get_bed, name='list_bed'),
    path('rumahsakit/list_reservasi', views.list_reservasi_rs, name='list_reservasi'),
    path('rumahsakit/update_reservasi/<str:row>/<str:row2>', views.update_reservasi_rs, name='update_reservasi'),
    path('rumahsakit/delete_reservasi/<str:row>/<str:row2>', views.delete_reservasi_rs, name='delete_reservasi'),
]
