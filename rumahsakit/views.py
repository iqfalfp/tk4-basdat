from django.shortcuts import render, redirect
from django.db import connection
from .forms import ReservasiRS, CreateRS,UpdateRS, UpdateReservasi
from datetime import datetime, timedelta
from django.contrib import messages
# Create your views here.

def form_create_rs(request):
    with connection.cursor() as cursor:
        response = {}
        response['error'] = False
        form = CreateRS(request.POST) 
        response['form'] = form
        if request.method == 'POST':
            if form.is_valid():
                kode_rs = request.POST.get('kode_rs')
                rujukan = form.cleaned_data['rujukan']
                if rujukan == True :
                    rujukan = 1
                if rujukan == False :
                    rujukan ==  0

               
            #    Insert Into RS
                cursor.execute("INSERT INTO RUMAH_SAKIT (kode_faskes, isrujukan)"
                               "VALUES ('{}','{}');".format(kode_rs,rujukan))

                return redirect('rumahsakit:list_rs') 
    form = CreateRS()
    args = {
        'form' : response['form'],
    }
    return render(request, 'create_rs.html', args)

def update_rs(request,row):
    value = ''
    with connection.cursor() as cursor:
        cursor.execute('SET SEARCH_PATH TO SIRUCO;')
        cursor.execute('SELECT * FROM RUMAH_SAKIT WHERE kode_faskes = %s;',[row])
        data_rs = cursor.fetchone()
        responses = {}
        responses['kode_faskes'] = data_rs[0]
        print(data_rs[0])
        responses['isrujukan'] = data_rs[1]
        print(data_rs[1])
        if data_rs[1] == '1':
            print("masuk case a")
            value = True
        if data_rs[1] == '0':
            print("masuk case b")
            value = False
        initialdict = {"kode_rs" : data_rs[0], "rujukan" : value}
        print(data_rs[1])
        print(initialdict)

    response = {}
    response['error'] = False
    form = UpdateRS(request.POST or None, initial=initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode_rs = row
            rujukan = form.cleaned_data['rujukan']
            print(rujukan)
            if rujukan == True :
                print("case a")
                rujukan = 1
            if rujukan == False :
                rujukan =  0
                print("case b")

               
            with connection.cursor() as cursor:
            #    Insert Into RS
                cursor.execute("UPDATE RUMAH_SAKIT SET kode_faskes = '{}', isrujukan = '{}' WHERE kode_faskes = '{}';".format(kode_rs,rujukan,kode_rs))

                return redirect('rumahsakit:list_rs')

    form = UpdateRS()
    args = {
        'form' : response['form'],
    }
    return render(request, 'update_rs.html', args)

def list_rs(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT RS.kode_faskes, RS.isrujukan FROM rumah_sakit AS RS, faskes AS F WHERE F.tipe = 'Rumah Sakit' AND RS.kode_faskes = F.kode;" )
    list_rs = cursor.fetchall()
    print(list_rs)
    print(len(list_rs))
    args = {'list_rs':list_rs}
    return render(request, 'list_rs.html', args)

def list_reservasi_rs(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed FROM reservasi_rs;" )
    list_reservasi = cursor.fetchall()
    print(list_reservasi)
    print(len(list_reservasi))
    args = {'list_reservasi':list_reservasi}
    return render(request, 'list_reservasi_rs.html', args)

def delete_reservasi_rs(request,row,row2):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute(f'''DELETE FROM RESERVASI_RS WHERE kodepasien = '{row}' AND tglmasuk = '{row2}';''')
    cursor.close()
    
    return redirect('rumahsakit:list_reservasi') 

def update_reservasi_rs(request,row,row2):
    # time = row2.strftime("%Y-%b-%d")
    # print(time)
    with connection.cursor() as cursor:
        cursor.execute('SET SEARCH_PATH TO SIRUCO;')
        cursor.execute(f'''SELECT * FROM RESERVASI_RS WHERE kodepasien = '{row}' AND tglmasuk = '{row2}';''')
        data_reservasi = cursor.fetchone()
        responses = {}
        responses['kodepasien'] = data_reservasi[0]
        print(data_reservasi[0])
        responses['tglmasuk'] = data_reservasi[1]
        responses['tglkeluar'] = data_reservasi[2]
        responses['koders'] = data_reservasi[3]
        responses['koderuangan'] = data_reservasi[4]
        responses['kodebed'] = data_reservasi[5]

        initialdict = {"NIK_Pasien" : data_reservasi[0], "tanggal_masuk" : data_reservasi[1], "kode_rs" : data_reservasi[3], "kode_ruangan" : data_reservasi[4], "kode_bed" : data_reservasi[5]}
        print(initialdict)

    response = {}
    response['error'] = False
    form = UpdateReservasi(request.POST or None, initial=initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            nik = form.cleaned_data['NIK_Pasien']
            tgl_masuk = form.cleaned_data['tanggal_masuk']
            tgl_keluar = form.cleaned_data['tanggal_keluar']
            kode_rs = form.cleaned_data['kode_rs']
            kode_ruangan = form.cleaned_data['kode_ruangan']
            kode_bed = form.cleaned_data['kode_bed']
            a = tgl_keluar.split('-')
            a_year = int(a[0])
            a_month = int(a[1])
            a_day = int(a[2])

            b = tgl_masuk.split('-')
            b_year = int(b[0])
            b_month = int(b[1])
            b_day = int(b[2])

            x_year = a_year - b_year
            x_month = a_month - b_month
            x_day = a_day - b_day

            time = (x_year * 365) + (x_month * 30) + (x_day)

            print(time)

            if(time>0):   
                with connection.cursor() as cursor:
                #   Insert Into RS
                    cursor.execute("UPDATE RESERVASI_RS SET kodepasien = '{}', tglmasuk = '{}', tglkeluar = '{}', koders = '{}', koderuangan = '{}', kodebed = '{}' WHERE kodepasien = '{}' AND tglmasuk = '{}';".format(nik,tgl_masuk,tgl_keluar,kode_rs,kode_ruangan,kode_bed,nik,tgl_masuk))
                    cursor.execute(f'''SELECT * FROM RESERVASI_RS WHERE kodepasien = '{nik}' AND tglmasuk = '{tgl_masuk}';''')
                    date_update = cursor.fetchone()
                    tglkeluar = date_update[2] 
                    tglmasuk = date_update[1]
                    d = tglkeluar-tglmasuk
                    tagihan = 500000 * d.days
                    print(tagihan)
                    
                    cursor.execute("SELECT * FROM TRANSAKSI_RS WHERE kodepasien = '{}' AND tglmasuk = '{}';".format(nik,tgl_masuk))
                    data_update = cursor.fetchone()
                    cursor.execute("UPDATE TRANSAKSI_RS SET idtransaksi = '{}', kodepasien = '{}', tanggalpembayaran = NULL , waktupembayaran = NULL , tglmasuk = '{}', totalbiaya = {}, statusbayar = '{}' WHERE kodepasien = '{}' AND tglmasuk = '{}';".format(data_update[0],data_update[1],data_update[4],tagihan,data_update[6], data_update[1], data_update[4]))
                    

                    return redirect('rumahsakit:list_reservasi')

               
    form = UpdateReservasi()
    args = {
        'form' : response['form'],
    }
    return render(request, 'update_reservasi.html', args)

def form_create_reservasi_rs(request):
    response = {}
    response['error'] = False
    form = ReservasiRS(request.POST or None) 
    response['form'] = form
    print("A")
    if request.method == 'POST':
        print("B")
        if form.is_valid():
            print("C")
            NIK_Pasien = form.cleaned_data['NIK_Pasien']
            print(NIK_Pasien)
            tanggal_masuk = request.POST.get('tanggal_masuk')
            tanggal_keluar = request.POST.get('tanggal_keluar')
            kode_rs = form.cleaned_data['kode_rs']
            kode_ruangan = form.cleaned_data['kode_ruangan']
            kode_bed = form.cleaned_data['kode_bed']
            print(tanggal_masuk)
            print(tanggal_keluar)
            print(kode_rs)
            print(kode_ruangan)
            print(kode_bed)
            a = tanggal_keluar.split('-')
            a_year = int(a[0])
            a_month = int(a[1])
            a_day = int(a[2])

            b = tanggal_masuk.split('-')
            b_year = int(b[0])
            b_month = int(b[1])
            b_day = int(b[2])

            x_year = a_year - b_year
            x_month = a_month - b_month
            x_day = a_day - b_day

            time = (x_year * 365) + (x_month * 30) + (x_day)

            print(time)

            if(time>0):  
                with connection.cursor() as cursor:
                #Insert into RESERVASI_RS
                    cursor.execute("INSERT INTO RESERVASI_RS VALUES ('{}','{}','{}','{}','{}','{}');".format(NIK_Pasien,tanggal_masuk, tanggal_keluar, kode_rs, kode_ruangan, kode_bed ))
                    return redirect('rumahsakit:list_reservasi')
            
    form = ReservasiRS()
    args = {
        'form' : response['form'],
    }
    return render(request, 'create_reservasi_rs.html', args)

def get_ruangan(request):
    rumah_sakit = request.GET.get('kode_rs')
    list_ruangan = []

    with connection.cursor() as cursor:
        cursor.execute("SELECT koderuangan FROM RUANGAN_RS WHERE koders = '{}' AND jmlbed > 0;".format(rumah_sakit))
        list_ruangan = cursor.fetchall()

    final = []
    for i in list_ruangan:
        val = (i[0],i[0])
        final.append(val)
    return render(request, 'list_ruangan.html', {'list_ruangan':final})

def get_bed(request):
    rumah_sakit = request.GET.get('kode_rs')
    ruangan = request.GET.get('kode_ruang')
    list_bed = []

    with connection.cursor() as cursor:
        cursor.execute("SELECT kodebed FROM BED_RS WHERE koders = '{}' AND koderuangan = '{}';".format(rumah_sakit,ruangan))
        list_bed = cursor.fetchall()

    final = [] 
    for i in list_bed:
        val = (i[0],i[0])
        final.append(val)
    return render(request, 'list_bed.html', {'list_bed':final})