from django import forms
from django.db import connection

class CreateRS(forms.Form):
    def get_all_faskes():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kode,kode FROM FASKES WHERE tipe = 'Rumah Sakit';")
        list_kode_rs = cursor.fetchall()
        cursor.close()

        return list_kode_rs

    kode_rs = forms.ChoiceField(label="Kode RS", choices= get_all_faskes(), required=True)

    rujukan = forms.BooleanField(label='rujukan',  required=False)

class UpdateRS(forms.Form):

    kode_rs = forms.CharField(label="Kode_rs", required=True, disabled=True)

    rujukan = forms.BooleanField(label='rujukan', required=False)

class ReservasiRS(forms.Form):

    def get_all_NIK():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select NIK,NIK FROM PASIEN;")
        list_nik = cursor.fetchall()
        cursor.close()

        return list_nik

    def get_all_kode_rs():
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        cursor.execute("select kode_faskes,kode_faskes FROM RUMAH_SAKIT;")
        list_kode_rs = cursor.fetchall()
        cursor.close()

        return list_kode_rs

    NIK_Pasien = forms.ChoiceField(label="NIK_Pasien", choices= get_all_NIK())

    tanggal_masuk = forms.CharField(label='Tanggal Masuk', widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'NIK Pasien',
        'type' : 'date',
        'required': True,
        }))

    tanggal_keluar = forms.CharField(label='Tanggal Keluar', widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tanggal Keluar',
        'type' : 'date',
        'required': True,
        }))
    
    kode_rs = forms.ChoiceField(label="Kode RS", choices= get_all_kode_rs(), required=True)

    kode_ruangan = forms.CharField(label="Kode Ruangan", required = True, widget=forms.Select(choices=[]))

    kode_bed = forms.CharField(label="Kode Bed", required=True, widget=forms.Select(choices=[]))

class UpdateReservasi(forms.Form):


    NIK_Pasien = forms.CharField(label="NIK_Pasien", required=True, disabled=True)

    tanggal_masuk = forms.CharField(label='Tanggal Masuk', required=True, disabled=True)

    tanggal_keluar = forms.CharField(label='Tanggal Keluar', widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tanggal Keluar',
        'type' : 'date',
        'required': True,
        }))
    
    kode_rs = forms.CharField(label="Kode RS", required=True, disabled=True)

    kode_ruangan = forms.CharField(label="Kode Ruangan", required = True, disabled=True)

    kode_bed = forms.CharField(label="Kode Bed", required=True, disabled=True)



