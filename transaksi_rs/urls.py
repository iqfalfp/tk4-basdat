from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'transaksi_rs'

urlpatterns = [
    path('transaksi_rs/list_transaksi_rs', views.list_transaksi_rs, name='list_transaksi_rs'),
    path('transaksi_rs/update_transaksi/<str:row>', views.update_transaksi_rs, name='update_transaksi_rs'),
    path('transaksi_rs/delete_transaksi/<str:row>', views.delete_transaksi_rs, name='delete_transaksi_rs')
]