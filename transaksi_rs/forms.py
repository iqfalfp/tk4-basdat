from django import forms
from django.db import connection

class UpdateTransaksiRS(forms.Form):

    idtransaksi =  forms.CharField(label='idtransaksi', disabled=True, required=True)

    kodepasien = forms.CharField(label="kodepasien", required=True, disabled=True)
    
    tanggalpembayaran = forms.CharField(label='tanggalpembayaran', disabled=True)
    
    waktupembayaran = forms.CharField(label="waktupembayaran", disabled=True)
    
    tglmasuk = forms.CharField(label='tglmasuk', required=True, disabled=True)
    totalbiaya = forms.CharField(label='totalbiaya', disabled=True, required=True)
    statusbayar = forms.CharField(label='statusbayar', required=True)
    