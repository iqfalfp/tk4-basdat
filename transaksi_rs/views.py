from django.shortcuts import render, redirect
from django.db import connection
from datetime import datetime, timedelta
from django.contrib import messages
from .forms import UpdateTransaksiRS

# Create your views here.

def list_transaksi_rs(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute("SELECT idtransaksi, kodepasien,tanggalpembayaran, waktupembayaran, tglmasuk, totalbiaya, statusbayar FROM TRANSAKSI_RS;" )
    list_transaksi_rs = cursor.fetchall()
    print(list_transaksi_rs)
    print(len(list_transaksi_rs))
    args = {'list_transaksi_rs':list_transaksi_rs}
    return render(request, 'transaksi_rs.html', args)

def update_transaksi_rs(request,row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT * FROM TRANSAKSI_RS WHERE idtransaksi = %s;',[row])
    data_transaksi_rs = cursor.fetchone()
    response = {}
    response['idtransaksi'] = data_transaksi_rs[0]
    response['kodepasien'] = data_transaksi_rs[1]
    response['tanggalpembayaran'] = data_transaksi_rs[2]
    response['waktupembayaran'] = data_transaksi_rs[3]
    response['tglmasuk'] = data_transaksi_rs[4]
    response['totalbiaya'] = data_transaksi_rs[5]
    response['statusbayar'] = data_transaksi_rs[6]
    print(response)
    cursor.close()
    initial_dict = {"idtransaksi":response['idtransaksi'], "kodepasien":response['kodepasien'], "tanggalpembayaran":response['tanggalpembayaran'], "waktupembayaran":response['waktupembayaran'], "tglmasuk":response['tglmasuk'], "totalbiaya":response['totalbiaya'],"statusbayar":response['statusbayar']}

    response['error'] = False
    form = UpdateTransaksiRS(request.POST or None, initial=initial_dict) 
    response['form'] = form
    if request.method == 'POST':
        print("test valid atau gak")
        print(request.POST.get('idtransaksi'))
        print(request.POST.get('kodepasien'))
        if form.is_valid():
            idtransaksi = form.cleaned_data['idtransaksi']
            kodepasien = form.cleaned_data['kodepasien']
            tanggalpembayaran = form.cleaned_data['tanggalpembayaran']
            waktupembayaran = form.cleaned_data['waktupembayaran']
            tglmasuk = form.cleaned_data['tglmasuk']
            totalbiaya = form.cleaned_data['totalbiaya']
            statusbayar = form.cleaned_data['idtransaksi']

            # UPDATE FASKES
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("UPDATE TRANSAKI_RS SET idtransaksi ='{}', kodepasien = '{}', tanggalpembayaran = '{}', waktupembayaran = '{}', tglmasuk = '{}', totalbiaya = '{}', statusbayar = '{}' WHERE idtransaksi = '{}';".format(idtransaksi,kodepasien,tanggalpembayaran,waktupembayaran,tglmasuk,totalbiaya,statusbayar,idtransaksi))
                print('test')
                return redirect('transaksI_rs:list_transaksi_rs') 
    
    form = UpdateTransaksiRS()
    args = {
        'form' : response['form'],
    } 

    return render(request, 'update_transaksi_rs.html', args)

def delete_transaksi_rs(request,row):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute(f'''DELETE FROM TRANSAKSI_RS WHERE idtransaksi = '{row}';''')
    cursor.close()
    
    return redirect('transaksi_rs:list_transaksi_rs') 
